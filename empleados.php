<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
header("Content-Type: application/json; charset=utf-8");

  include "library/config.php";

  $query = $mysqli->query("SELECT * FROM empleados");

  $rows = array();
  while($r = mysqli_fetch_assoc($query)) {
      $rows[] = $r;
  }

  $result = json_encode(array('success'=>true, 'msg'=>'Empleados', 'result'=>$rows));
  echo $result;
?>