import { Component, OnInit } from '@angular/core';
import { AlertController,LoadingController} from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  constructor(public http: HttpClient,public alertController: AlertController,public loadingController: LoadingController) { }

  ngOnInit() {
    this.getEmpleados()
  }
  empleados: any[] = [];
    
    async getEmpleados() {
      let loading = await this.loadingController.create({
        message: 'Cargando Datos...'
      });
  
    //mostramos el loading para que se bloquee la pantalla
    //y se muestre el mensaje de cargando
      await loading.present();

  
      //Se agrega  los encabezados para que se mande en formato json, 
      //por que nuestro api solocita estos datos en formato json
      const headers = new HttpHeaders();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json');
      const options = { headers: headers, withCredintials: false };
      
      //Se realiza la peticion a la api
      const data = await this.http.get("http://127.0.0.1:8888/huellitas_api/empleados.php", options).toPromise();
      this.empleados = data['result'];
      //se oculta el loading de cargando 
      await loading.dismiss();
    }
    
    
}