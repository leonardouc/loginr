



app.module.ts

import { HttpClientModule } from '@angular/common/http';


login.page.ts


import { HttpClient, HttpHeaders } from '@angular/common/http';


  constructor(public http: HttpClient, public fb: FormBuilder,
    public alertController: AlertController,
    public navCtrl: NavController, public loadingController: LoadingController) {
    this.formularioLogin = this.fb.group({
      'nombre': new FormControl("", Validators.required),
      'password': new FormControl("", Validators.required),
    })

  }

  async ingresar() {
          var f = this.formularioLogin.value;
          
    let loading = await this.loadingController.create({
      message: 'Ingresando...'
    });

  //mostramos el loading para que se bloquee la pantalla
  //y se muestre el mensaje de cargando
    await loading.present();
    


    //Se agrega  los encabezados para que se mande en formato json, 
    //por que nuestro api solocita estos datos en formato json
    const headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    const options = { headers: headers, withCredintials: false };

    //Se crea el objeto en formato json
    let postData = {
      "nombre": f.nombre,
      "password": f.password
    }
    //Se realiza la peticion a la api
    const data = await this.http.post("http://127.0.0.1:8888/huellitas_api/login.php", postData, options).toPromise();

    //se oculta el loading de cargando 
    await loading.dismiss();

    //se valida se el success que devuelve nuestro api es verdadero
    //si es verdadero se direcciona al inicio
    //si es falso se muestra un alert informandole
    if(data['success']){
      this.navCtrl.navigateRoot('inicio');
    } else {
      const alert = await this.alertController.create({
        header: 'Mensage',
        message: data['msg'],
        buttons: ['Aceptar']
      });
  
    //se presenta el alert para informarle al usuario que fallo su ingreso
      await alert.present();
    }
  

  }
}

